<?php
/**
 * Created by PhpStorm.
 * User: obadjahauser
 * Date: 03.03.18
 * Time: 10:19
 */

namespace Vitd\ContentWidget\Api\Data;


interface ContentWidgetConstInterface
{
    # PARAMETER NAMEs
    const PARAM_URL = 'vitd_url';
    const PARAM_TIMEOUT = 'vitd_timeout';
    const PARAM_TYPE = 'vitd_type';
    const PARAM_CONTAINER = 'vitd_container';
    const PARAM_STRING_CONVERSION = 'vitd_string_conversion';
    const PARAM_NLBR = 'vitd_nlbr';
    const PARAM_ESCAPE_CONTENT = 'vitd_escape_content';
    const PARAM_HTTP_METHOD = 'vitd_http_method';
    const PARAM_STORECODE = 'vitd_storecode';
    const PARAM_STORECODE_PARAM = 'vitd_storecode_param';
    const PARAM_FORCE_RELOAD = 'vitd_force_reload';
    const PARAM_FORCE_INCLUDE = 'vitd_force_include';
    const PARAM_IFRAME_WIDTH = 'vitd_iframe_width';
    const PARAM_IFRAME_HEIGHT = 'vitd_iframe_height';
    const PARAM_SERVER_PROTOCOL = 'vitd_server_protocol';
    const PARAM_SERVER_NAME = 'vitd_server_name';
    const PARAM_CONTENT_CLASS = 'vitd_content_class';
    const PARAM_USE_HOSTSTABLE = 'vitd_use_hoststable';
    const PARAM_ACCESS_RULE = 'vitd_access_rule';

    # DECLARE DEFAULT VALUES - is OPTIONAL
    #const VALUE_NLBR = false;
    #const VALUE_STORECODE = true;
    #const VALUE_FORCE_RELOAD = false;
    #const VALUE_FORCE_INCLUDE = false;
    #const VALUE_ESCAPE_CONTENT = false;
    #const VALUE_TIMEOUT = 10;
    #const VALUE_TYPE = 'url';
    #const VALUE_STORECODE_PARAM = 'storecode';
    #const VALUE_HTTP_METHOD = 'GET';
    #const VALUE_CONTENT_CLASS = 'vitd_content';
    #const VALUE_IFRAME_WIDTH = '100%';
    #const VALUE_IFRAME_HEIGHT = '100%';

}