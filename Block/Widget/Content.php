<?php

namespace Vitd\ContentWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Vitd\ContentWidget\Api\Data\ContentWidgetConstInterface;

#use Magento\Framework\View\Element\Template\Context;


class Content extends Template implements BlockInterface, ContentWidgetConstInterface
{
    protected $_template = "widget/content.phtml";

    private $cached = [];

    /**
     * @var \Magento\Store\Model\StoreManagerInterface $_storeManager
     */

    public function getContainerClass()
    {
        return $this->getVitdContentClass();
    }

    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    public function useStoreCode()
    {
        return $this->getUseVitdStorecode();
    }

    public function getStoreCodePrefix()
    {
        return $this->getVitdStorecodeParam();
    }

    public function isAccessAllowed()
    {
        return $this->isVitdAccessAllowed();
    }

    public function getContent($fileOrUrl = '')
    {
        $content = '';

        if (empty($fileOrUrl)) {
            if ($this->isPostRequest()) {
                $fileOrUrl = $this->prepareLinkForPost();
            } else {
                $fileOrUrl = $this->prepareLink();
            }
        }

        if ($this->isOutputDisabled()) {
            return sprintf('<div style="color: red;">%s</div>', htmlspecialchars($fileOrUrl));
        }

        if ((bool)$this->getVitdForceInclude() && !$this->isUrlRequest()) {
            try {
                $timeout = ini_get('max_execution_time');
                set_time_limit($this->getVitdTimeout());
                ob_start();
                @include($fileOrUrl);
                $content = ob_get_clean();
                set_time_limit($timeout);
            } catch (\Exception $e) {
                $content = sprintf("Processing failed: %s", $e->getMessage());
            }
        } else {
            $context = [
                'http' => [
                    'method' => $this->getVitdHttpMethod(),
                    'timeout' => $this->getVitdTimeout()
                ]
            ];

            if ($this->isPostRequest()) {
                $context['http']['content'] = http_build_query($fileOrUrl['params']);
                $fileOrUrl = $fileOrUrl['url'];
            }

            try {
                $content = file_get_contents($fileOrUrl, false, stream_context_create($context));
                switch (connection_status()) {
                    case CONNECTION_ABORTED:
                        $content = '';
                        break;
                    case CONNECTION_TIMEOUT:
                    case 3:
                        $content = '';
                        throw new \Exception('Connection timeout', 1520066554);
                        break;
                }
            } catch (\Exception $e) {
                $content = sprintf("Processing failed: %s", $fileOrUrl);
            }
        }

        if (!empty($content)) {
            $content = $this->processContent($content);
        }

        return $content;
    }

    public function prepareLinkForIframe($fileOrUrl = '')
    {
        if (empty($fileOrUrl)) {
            $fileOrUrl = $this->prepareLink();
        }

        if ($this->getVitdType() === 'file') {
            if ((bool)$this->getVitdForceInclude()) {
                $content = 'data:' . htmlspecialchars($this->getContent());
            } else {
                if (stripos($fileOrUrl, $_SERVER['DOCUMENT_ROOT']) === 0) {
                    $content = str_replace($_SERVER['DOCUMENT_ROOT'], '', $fileOrUrl);
                } else {
                    $content = '';
                }
            }
        } else {
            $content = $fileOrUrl;
        }

        return $content;
    }

    public function getIframeAttributes()
    {
        return 'width="' . $this->getVitdIframeWidth() . '" '
            . 'height="' . $this->getVitdIframeHeight() . '" '
            . 'frameborder="no" ';
    }


########################################################################################################################
### PRIVATE
########################################################################################################################

    public function isOutputDisabled()
    {
        return !empty($this->_scopeConfig->getValue('vitd_contentwidget/general/disabled'));
    }

    /**
     * @return bool
     */
    private function getUseVitdStorecode()
    {
        return (bool)$this->getParam('storecode') ?: true;
    }

    /**
     * @return bool
     */
    private function getUseVitdHostTable()
    {
        return (bool)$this->getParam('use_hoststable') ?: true;
    }

    /**
     * @return bool
     */
    private function getVitdForceInclude()
    {
        return (bool)$this->getParam('force_include') ?: false;
    }

    /**
     * @return bool
     */
    private function getVitdForceReload()
    {
        return (bool)$this->getParam('force_reload') ?: false;
    }

    /**
     * @return bool
     */
    private function getVitdEscapeContent()
    {
        return (bool)$this->getParam('escape_content') ?: false;
    }

    /**
     * @return bool
     */
    private function getVitdNlbr()
    {
        return (bool)$this->getParam('nlbr') ?: false;
    }

    /**
     * @return int
     */
    private function getVitdTimeout()
    {
        $value = (int)$this->getParam('timeout');
        if ($value <= 0) {
            $value = defined('VALUE_TIMEOUT') ? (int)static::VALUE_TIMEOUT : 10;
            if ($value <= 0) {
                $value = 10;
            }
        }
        return $value;
    }

    /**
     * @return string
     */
    private function getVitdStorecodeParam()
    {
        $value = (string)$this->getParam('storecode_param');
        return $value ?: 'storecode';
    }

    /**
     * @return bool
     */
    private function isVitdAccessAllowed()
    {
        /** @var \Magento\Customer\Model\Session $session */
        $rule = (string)$this->getParam('access_rule');

        if (in_array($rule, ['guest', 'user'])) {
            $session = \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Customer\Model\Session::class);
            if ($rule === 'guest') {
                return !$session->isLoggedIn();
            } elseif ($rule === 'user') {
                return $session->isLoggedIn();
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return string
     */
    private function getVitdType()
    {
        $value = strtolower((string)$this->getParam('type'));
        if (!in_array($value, ['url', 'file'])) {
            $value = 'url';
        }
        return $value;
    }

    /**
     * @return string
     */
    private function getVitdHttpMethod()
    {
        $value = strtoupper((string)$this->getParam('http_method'));
        if (!in_array($value, ['GET', 'POST'])) {
            $value = 'GET';
        }
        return $value;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdUrl()
    {
        return $this->getPrefixUrl((string)$this->getParam('url'));
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdContainer()
    {
        $value = strtoupper((string)$this->getParam('container'));
        if (!empty($value) && !in_array($value, ['DIV', 'IFRAME'])) {
            $value = 'DIV';
        }
        return $value ?: '';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdStringConversion()
    {
        $value = strtolower((string)$this->getParam('string_conversion'));
        if ($value === 'no') {
            $value = '';
        }
        return $value ?: '';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdIframeWidth()
    {
        $value = (string)$this->getParam('iframe_width');
        return $value ?: '100%';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdIframeHeight()
    {
        $value = (string)$this->getParam('iframe_height');
        return $value ?: '100%';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdContentClass()
    {
        $value = (string)$this->getParam('content_class');
        return $value ?: 'vitd_content';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getVitdServerProtocol()
    {
        $value = $this->getParam('server_protocol');
        return $value ?: 'http' . (array_key_exists('HTTPS', $_SERVER) ? 's' : '');
    }

    /**
     * @return string
     */
    private function getVitdServerName()
    {
        $value = $this->getParam('server_name');
        return $value ?: $_SERVER['SERVER_NAME'] . ((int)$_SERVER['SERVER_PORT'] !== 80 ? ':' . $_SERVER['SERVER_PORT'] : '');
    }

    /**
     * @return bool
     */
    private function isUrlRequest()
    {
        return $this->getVitdType() === 'url';
    }

    /**
     * @return bool
     */
    private function isPostRequest()
    {
        return $this->isUrlRequest() && $this->getVitdHttpMethod() === 'POST';
    }

    /**
     * @return bool
     */
    private function isUseHostsTable()
    {
        $scopeConfig = (int)$this->_scopeConfig->getValue('vitd_contentwidget/general/use_hoststable');
        return $this->isUrlRequest() && (int)$this->getUseVitdHostTable() && $scopeConfig === 1;
    }

    /**
     * @param string $calledUrl
     * @param string|null $localServerIp
     * @return string
     */
    private function getMatchingHosttable(string $calledUrl, string $localServerIp = null)
    {
        if (empty($localServerIp)) {
            $localServerIp = $_SERVER['SERVER_ADDR'];
        }

        $scopeConfig = (string)$this->_scopeConfig->getValue('vitd_contentwidget/general/hosts_table');
        $table = [];

        if (!empty($scopeConfig)) {
            $lines = explode("\r\n", $scopeConfig);
            foreach ($lines as $line) {
                $keyValue = explode('|', $line);
                if (count($keyValue) === 2) {
                    $table[trim($keyValue[0])] = trim($keyValue[1]);
                }
            }
        }

        if (key_exists($localServerIp, $table)) {
            $calledUrl = $table[$localServerIp];
        }

        return $calledUrl;
    }

    private function getPrefixUrl(string $calledUrl = '')
    {
        if ($this->isUseHostsTable()) {
            $scopeConfig = (string)$this->_scopeConfig->getValue('vitd_contentwidget/general/prefix_url');
            $calledUrl = str_replace($scopeConfig, '', $calledUrl); #Clean called url
            $calledUrl = $this->getMatchingHosttable($scopeConfig) . $calledUrl;
        }
        return $calledUrl;
    }

    /**
     * @param string $key
     * @return null|bool|integer|float|string
     */
    private function getParam($key)
    {
        $return = null;

        $key = strtolower(str_replace('vitd_', '', $key));
        if (!empty($key)) {
            $key = strtoupper($key);
            $xmlParamter = constant('static::PARAM_' . $key);

            if (key_exists($key, $this->cached)) {
                $return = $this->cached[$key];
            } else {
                $defaultValue = defined('static::VALUE_' . $key) ? constant('static::VALUE_' . $key) : null;
                $customValue = null;

                if (!empty($xmlParamter)) {
                    if ($this->hasData($xmlParamter)) {
                        $customValue = $this->getData($xmlParamter);
                    }
                }

                $return = $customValue === NULL ? $defaultValue : $customValue;
                $this->cached[$key] = $return;
            }

        }

        return $return;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function prepareLink()
    {
        $fileOrUrl = trim($this->getVitdUrl());

        if ($this->isUrlRequest()) {
            $isAbsolute = stripos($fileOrUrl, 'http://') === 0
                || stripos($fileOrUrl, 'https://') === 0;
            $isAbsoluteLocal = stripos($fileOrUrl, '/') === 0;

            $useCurrentProtocol = stripos($fileOrUrl, '//') === 0;

            if ($isAbsolute || $isAbsoluteLocal) {
                if ($useCurrentProtocol) {
                    $fileOrUrl = $this->getVitdServerProtocol() . ':' . $fileOrUrl;
                } elseif ('iframe' !== $this->getVitdContainer() && $isAbsoluteLocal) {
                    $fileOrUrl = $this->getVitdServerProtocol() . '://' . $this->getVitdServerName() . $fileOrUrl;
                }
            }

            if ($this->useStoreCode()) {
                if (stripos($fileOrUrl, '?') !== false) {
                    $fileOrUrl .= '&' . $this->getStoreCodePrefix() . '=' . $this->getStoreCode();
                } else {
                    $fileOrUrl .= '?' . $this->getStoreCodePrefix() . '=' . $this->getStoreCode();
                }
            }

            if ($this->getVitdForceReload()) {
                if (stripos($fileOrUrl, '?') !== false) {
                    $fileOrUrl .= '&' . time();
                } else {
                    $fileOrUrl .= '?' . time();
                }
            }

        } else {

            if (stripos($fileOrUrl, '/') !== 0) {
                $fileOrUrl = $_SERVER['DOCUMENT_ROOT'] . '/' . $fileOrUrl;
            }

        }

        return $fileOrUrl;
    }

    /**
     * @return array [url,params[]]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function prepareLinkForPost()
    {
        $result = [
            'url' => '',
            'params' => []
        ];

        if ($this->isPostRequest()) {
            $fileOrUrl = trim($this->getVitdUrl());
            $container = $this->getVitdContainer();

            $isAbsolute = stripos($fileOrUrl, 'http://') === 0
                || stripos($fileOrUrl, 'https://') === 0;
            $isAbsoluteLocal = stripos($fileOrUrl, '/') === 0;

            $useCurrentProtocol = stripos($fileOrUrl, '//') === 0;

            if ($isAbsolute || $isAbsoluteLocal) {
                if ($useCurrentProtocol) {
                    $fileOrUrl = $this->getVitdServerProtocol() . ':' . $fileOrUrl;
                } elseif ('iframe' !== $container && $isAbsoluteLocal) {
                    $fileOrUrl = $this->getVitdServerProtocol() . '://' . $this->getVitdServerName() . $fileOrUrl;
                }
            }

            if ($this->useStoreCode()) {
                $result['params'][$this->getStoreCodePrefix()] = $this->getStoreCode();
            }

            if ($this->getVitdForceReload()) {
                $result['params']['ts'] = time();
            }

            $result['url'] = $fileOrUrl;

        } else {
            return [];
        }

        return $result;
    }

    /**
     * @param $content
     * @return array|bool|mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function processContent($content)
    {
        if ($this->getVitdEscapeContent()) {
            $_content = htmlentities($content, ENT_QUOTES | ENT_SUBSTITUTE);
        } else {
            $_content = $content;
        }

        if ($this->getVitdNlbr()) {
            $_content = nl2br($_content);
        }

        switch ($this->getVitdStringConversion()) {
            case 'utf8':
                $_content = \ForceUTF8\Encoding::toUTF8($_content);
                break;
            case 'win1252':
                $_content = \ForceUTF8\Encoding::toWin1252($_content);
                break;
            case 'fix':
                $_content = \ForceUTF8\Encoding::fixUTF8($_content);
                break;
            case 'fix1252':
                $_content = \ForceUTF8\Encoding::UTF8FixWin1252Chars($_content);
                break;
        }

        return $_content;
    }

}