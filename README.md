# VITD Content Widget


## Parameters

| Type | Parameter | Description |
|------|-----------|-------------|
| string | vitd_type | 'url' or 'file'. |
| string | vitd_url | This value will used for both, URL or FILE. |
| select | vitd_force_include | 0 or 1. The file will included with `include()`, on else will `file_get_contents()` used. |
| select | vitd_force_reload | 0 or 1. This will adding a timestamp to the url. |
| select | vitd_container | '', 'div' or 'iframe'. The content can have an clousure 'div' or 'iframe'. |
| number | vitd_iframe_width | Define the IFRAME width. |
| number | vitd_iframe_height | Define the IFRAME height. |
| select | vitd_string_conversion |  '' => no Conversion, 'utf8' => to UTF-8, 'win1252' => to Win1252 / Latin1 / ISO-8859, 'fix' => fix UTF-8 or 'fix1252' => fix UTF-8 with mixed Win1252. |
| select | vitd_nlbr | 0 or 1. This will replace all newlines to `<br/>` |
| select | vitd_escape_content | 0 or 1. This will escape the content with `htmlentities($content, ENT_QUOTES | ENT_SUBSTITUTE)` |
| number | vitd_timeout | default is ***10*** seconds |
| select | vitd_http_method | 'GET' or 'POST' default is ***GET*** |
| select | vitd_use_hoststable | 0 or 1. Use prefix url and hosts table configuration from the store configuration |


## Usage

Example usage in XML  
```xml  
<block class="Vitd\ContentWidget\Block\Widget\Content">
    <arguments>
        <argument name="vitd_type" xsi:type="string">url</argument>
        <argument name="vitd_url" xsi:type="string">/customContent.php</argument>
    </arguments>
</block>
```  

Example usage in Page  
```  
// Minimlistic usage (defautl is type 'url'
{{widget type="Vitd\\ContentWidget\\Block\\Widget\\Content" vitd_url="/customContent.php"}}
```  


##Release Notes

### v1.4.0 (2018-06-19)  

- New: Backend configuration to disable the output of the widget and show instead the URL or FILENAME

### v1.3.0 (2018-06-05)

- New: store configuration for the widget in path -> /stores/configuration/vitamin d/content widget  
- New parameter: ***vitd_use_hoststable***

### v1.2.2 (2018-03-06)

- Bug: build url has not accepted absolute paths.

### v1.2.0 (2018-03-03)   
 
- New parameter ***vitd_timeout***
- New parameter ***vitd_http_method***
- Catch exceptions for *file_get_contents* or *scripts*. Content will replaced with message, on abort by client will the content only cleared.
- Constants moved to `ContentWidgetConstInterface`, this file has now all predefined default values (see for more in this class)
- Code optimized

